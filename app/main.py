from flask import Flask, render_template
from kubernetes import client, config


app = Flask(__name__)


def initkube():
    try:
        config.load_kube_config()
    except (FileNotFoundError,TypeError):
        config.load_incluster_config()
    return client.AppsV1Api()
    

@app.route('/')
def home():
    return render_template("index.html")

@app.route('/repo/')
def repo():
    v1 = initkube()
    selector="app=repo-merge"
    deployments = v1.list_namespaced_deployment("gitlab",label_selector=selector)
    repos = [i.metadata.labels['release'] for i in deployments.items]
    return render_template("repos.html",repos=repos,name="Repositories",base="repo")

@app.route('/build/')
def build():
    v1 = initkube()
    selector="app=artifact-nginx"
    deployments = v1.list_namespaced_deployment("gitlab",label_selector=selector)
    repos = [i.metadata.labels['release'] for i in deployments.items]
    return render_template("repos.html",repos=repos,name="RPM Builds",base="build")

if __name__ == '__main__':
    app.run(debug=True)
